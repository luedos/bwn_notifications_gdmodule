#include "precompiled_notifications.hpp"

#include "register_types.h"

#include "bwn_notifications/notificationSubscriberNode.hpp"
#include "bwn_notifications/notificationRegistry.hpp"

void register_bwn_notifications_types()
{
	ClassDB::register_class<bwn::NotificationSubscriberNode>();
}

void unregister_bwn_notifications_types()
{}

void initialize_bwn_notifications_module(const ModuleInitializationLevel _level)
{
	if (_level == ModuleInitializationLevel::MODULE_INITIALIZATION_LEVEL_CORE)
	{
		bwn::NotificationRegistry::createSingleton();
	}
	else if (_level == ModuleInitializationLevel::MODULE_INITIALIZATION_LEVEL_SCENE)
	{
		GDREGISTER_CLASS(bwn::NotificationSubscriberNode);
		GDREGISTER_CLASS(bwn::NotificationBase);
	}
}

void uninitialize_bwn_notifications_module(const ModuleInitializationLevel _level)
{
	if (_level == ModuleInitializationLevel::MODULE_INITIALIZATION_LEVEL_CORE)
	{
		bwn::NotificationRegistry::destroySingleton();
	}
}
