#pragma once

#include <scene/main/node.h>
#include <core/object/ref_counted.h>
#include <core/string/ustring.h>

namespace bwn
{
	class NotificationGodotInterface;
	class NotificationBase;

	class NotificationSubscriberNode : public Node
	{
		GDCLASS(NotificationSubscriberNode, Node);

		friend NotificationGodotInterface;

		//
		// Construction and destruction.
		//
	public:
		NotificationSubscriberNode();
		virtual ~NotificationSubscriberNode() override;

		//
		// Godot methods.
		//
	protected:
		static void _bind_methods();
		void _notification(const int _notification);

		//
		// Private methods. 
		//
	private:
		void setNotificationType(const String& _type);
		const String& getNotificationType() const;
		void setNotificationName(const String& _type);
		const String& getNotificationName() const;
		void setSubscribeGlobal(const bool _global);
		bool getSubscribeGlobal() const;
		
		void signal(const Ref<NotificationBase>& _notification);
		void unsubscribe();
		void subscribe();

		//
		// Private methods.
		//
	private:
		String m_notificationType;
		String m_notificationName;
		bool m_subscribeGlobal = false;
	};
} // namespace bwn

