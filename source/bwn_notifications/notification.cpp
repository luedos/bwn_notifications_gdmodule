#include "precompiled_notifications.hpp"

#include "bwn_notifications/notification.hpp"

bwn::NotificationBase::NotificationBase() = default;

bwn::NotificationBase::NotificationBase(const String& _name)
	: m_name( _name )
{}

void bwn::NotificationBase::_bind_methods()
{
	ClassDB::bind_method(D_METHOD("signal", "node"), &NotificationBase::signal);
	ClassDB::bind_method(D_METHOD("signal_immediate", "node"), &NotificationBase::signalImmediate);

	BWN_BIND_PROPERTY("notification_name", &NotificationBase::setNotificationName, &NotificationBase::getNotificationName);
}

const String& bwn::NotificationBase::getNotificationName() const
{
	return m_name;
}

void bwn::NotificationBase::setNotificationName(const String& _name)
{
	m_name = _name;
}

void bwn::NotificationBase::signal(const Node*const _node)
{
	signalV(_node);
}

void bwn::NotificationBase::signalImmediate(const Node*const _node)
{
	signalImmediateV(_node);
}
