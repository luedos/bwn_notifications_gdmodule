#pragma once

#include "bwn_core/functional/callback.hpp"

#include <core/string/ustring.h>
#include <core/object/ref_counted.h>

#include <vector>

namespace bwn
{
	// Helpers which automatically create notification and signal it.
	template<typename NotificationT, typename...ArgTs>
	void signalNotification(const Node*const _node, ArgTs&&..._args);
	template<typename NotificationT, typename...ArgTs>
	void signalNotificationImmediate(const Node*const _node, ArgTs&&..._args);
	
	class NotificationBase : public RefCounted
	{
		GDCLASS(NotificationBase, RefCounted)

		//
		// Construction and destruction.
		//
	public:
		NotificationBase();
		NotificationBase(const String& _name);

		//
		// Godot methods.
		//
	protected:
		static void _bind_methods();

		//
		// Public interface.
		//
	public:
		// Returns name of the notification.
		const String& getNotificationName() const;
		// Resets name for the notification.
		void setNotificationName(const String& _name);

		// Signals self to all global subscibers. If provided node is not nullptr,
		// then notification will also be signaled to all listeners subscribed on this node or it's parents.
		// Signal by default is delayed and will be processed then NotificationManager will process them (generally at the end of frame).
		void signal(const Node*const _node = nullptr);
		// Same as 'signal' but actually will be distributed to subscribers immediately.
		void signalImmediate(const Node*const _node = nullptr);

		//
		// Protected methods.
		//
	private:
		virtual void signalV(const Node*const _node) {}
		virtual void signalImmediateV(const Node*const _node) {}

		//
		// Private members.
		//
	private:
		String m_name;
	};

	template<typename NotificationClassT, typename NotificationBaseClassT>
	class NotificationInterface : public NotificationBaseClassT
	{
		//
		// Public typedefs.
		//
	public:
		using NotificationBaseClassT::NotificationBaseClassT;

		//
		// Private classes.
		//
	private:
		// Single subscription which is created for the every subscriber.
		struct Subscription;
		// Notification static data which includes all the subscriptions.
		struct SubscriptionsData;

		//
		// Public typedefs.
		//
	public:
		using InheritedNotificationType = NotificationBaseClassT;
		using NotificationCallback = bwn::callback<void(const Ref<NotificationClassT>&)>;

		//
		// Public interface.
		//
	public:
		// Registers callback to be called when notification is signaled.
		// Because this subscription is global, callback will be triggered even in case if notification was sent to specific node.
		// If name is provided, the callback will be signaled only for the notification with the same name.
		static void subscribeGlobal(const Object& _listener, const NotificationCallback& _callback, const String& _name = String());
		// Registers callback to be called when notification is signaled.
		// Because of the provided node, callback will be signaled only if notification was sent to this node or any of it's children.
		// If name is provided, the callback will be signaled only for the notification with the same name.
		static void subscribeForNode(const Object& _listener, const Node& _node, const NotificationCallback& _callback, const String& _name = String());
		// Removes all subscriptions for the specific listener.
		static void unsubscribeAll(const Object& _listener);
		// Removes global subscription of specific listener for specific name.
		// Empty name here considered as a separate name, so if you had two subscriptions, one with empty name and one with not empty,
		// unsubscribing from empty name will not remove subscription with some name.
		static void unsubscribeGlobal(const Object& _listener, const bwn::string_view& _name = bwn::string_view());
		// Removes subscription on specific node for specific listener for specific name.
		// Empty name here considered as a separate name, so if you had two subscriptions, one with empty name and one with not empty,
		// unsubscribing from empty name will not remove subscription with some name.
		static void unsubscribeFromNode(const Object& _listener, const Node& _node, const bwn::string_view& _name = bwn::string_view());

		//
		// Protected methods.
		//
	protected:
		// Same as 'signalGlobal' but actually will be distributed to subscribers immediately.
		static void signalImmediateStatic(NotificationClassT& _notification, const Node*const _node);
		
		//
		// Private methods.
		//
	private:
		virtual void signalImmediateV(const Node*const _node) override;

		//
		// Private method.
		//
	private:
		static SubscriptionsData& getSubscriptionsData();
	};

	template<typename NotificationManagerT>
	class Notification : public NotificationBase
	{
		//
		// Public typedefs.
		//
	public:
		using NotificationManagerType = NotificationManagerT;
		using NotificationBase::NotificationBase;

		//
		// Private methods.
		//
	private:
		virtual void signalV(const Node*const _node) override;
	};

} // namespace bwn

#include "bwn_notifications/notification.inl"