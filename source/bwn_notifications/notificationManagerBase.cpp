#include "precompiled_notifications.hpp"

#include "bwn_notifications/notificationManagerBase.hpp"
#include "bwn_notifications/notification.hpp"

struct bwn::NotificationManagerBase::NotificationRecord
{
	NotificationRecord(const Ref<NotificationBase>& _notification, const Node*const _node)
		: notification(_notification)
		, node(_node)
	{}

	Ref<NotificationBase> notification;
	const Node* node = nullptr;
};

bwn::NotificationManagerBase::NotificationManagerBase(const int _processNotification)
	: m_stack()
	, m_stackLock()
	, m_processNotification( _processNotification )
{}
bwn::NotificationManagerBase::~NotificationManagerBase() = default;

void bwn::NotificationManagerBase::_notification(const int _notification)
{
	if (_notification == m_processNotification)
	{
		process();
	}
}

void bwn::NotificationManagerBase::pushNotification(const Ref<NotificationBase>& _notification, const Node*const _node)
{
	std::unique_lock<bwn::spinlock> lock(m_stackLock);
	m_stack.emplace_back(_notification, _node);
}

void bwn::NotificationManagerBase::process()
{
	{
		std::unique_lock<bwn::spinlock> lock( m_stackLock );
		m_cacheStack.swap(m_stack);
	}

	for (NotificationRecord& record : m_cacheStack)
	{
		record.notification->signalImmediate(record.node);
	}

	m_cacheStack.clear();
}