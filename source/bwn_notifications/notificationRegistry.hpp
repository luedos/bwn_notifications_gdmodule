#pragma once

#include "bwn_core/types/singleton.hpp"
#include "bwn_core/types/stringId.hpp"
#include "bwn_core/functional/callback.hpp"

#include <unordered_map>

class Object;

namespace bwn
{

	class NotificationSubscriberNode;
	class NotificationBase;

	class NotificationGodotInterface
	{
		//
		// Private classes.
		//
	private:
		template<typename NotificationT>
		struct SignalConverterFunctor
		{			
			SignalConverterFunctor(NotificationSubscriberNode& _subscriber);
			void operator()(const Ref<NotificationT>& _notification) const;
			bool operator==(const SignalConverterFunctor& _other) const;

			NotificationSubscriberNode* m_subscriber = nullptr;
		};

		//
		// Public typedefs.
		//
	public:
		using GlobalSubscriberFunc = void(*)(NotificationSubscriberNode& _subscriber, const String& _name);
		using NodeSubscriberFunc = void(*)(NotificationSubscriberNode& _subscriber, const Node& _node, const String& _name);
		using UnsubscriberFunc = void(*)(NotificationSubscriberNode& _subscriber);
		
		//
		// Construction and destruction.
		//
	private:
		NotificationGodotInterface() = default;

		//
		// Public interface.
		//
	public:
		void subscribeGlobal(NotificationSubscriberNode& _subscriber, const String& _name) const;
		void subscribeForNode(NotificationSubscriberNode& _subscriber, const Node& _node, const String& _name) const;
		void unsubscribeAll(NotificationSubscriberNode& _subscriber) const;

		template<typename NotificationT>
		static NotificationGodotInterface createInterface();

		//
		// Private methods.
		//
	private:
		template<typename NotificationT>
		static void notificationGlobalSubscriberTemplate(NotificationSubscriberNode& _subscriber, const String& _name);
		template<typename NotificationT>
		static void notificationNodeSubscriberTemplate(NotificationSubscriberNode& _subscriber, const Node& _node, const String& _name);
		template<typename NotificationT>
		static void notificationUnsubscriberTemplate(NotificationSubscriberNode& _subscriber);

		//
		// Private members.
		//
	private:
		GlobalSubscriberFunc m_notificationGlobalSubscriber = nullptr;
		NodeSubscriberFunc m_notificationNodeSubscriber = nullptr;
		UnsubscriberFunc m_notificationUnsubscriber = nullptr;
	};

	class NotificationRegistry : public ManualSingleton<NotificationRegistry>
	{
		//
		// Private typedefs.
		//
	private:
		using RegistrationMap = std::unordered_map<bwn::StringId, NotificationGodotInterface>;

		//
		// Construction and destruction.
		//
	public:
		NotificationRegistry();
		~NotificationRegistry();

		//
		// Public interface.
		//
	public:
		template<typename NotificationT>
		void registerNotification()
		{
			const String classString = NotificationT::get_class_static();
			const StringId id = StringId(bwn::wrap_view(classString));
			const RegistrationMap::const_iterator it = m_registrationMap.find(id);
			BWN_ASSERT_WARNING_COND(it == m_registrationMap.end(), "Attempt to register already registered notification {}.", classString);
			
			if (it == m_registrationMap.end())
			{
				m_registrationMap.emplace(id, NotificationGodotInterface::createInterface<NotificationT>());
			}
		}

		const NotificationGodotInterface* findInterface(const StringId& _id) const;
		
		//
		// Private members.
		//
	private:
		// A map with functions, which is used to register for notifications.
		RegistrationMap m_registrationMap;
	};

} // namespace bwn

#include "bwn_notifications/notificationRegistry.inl"