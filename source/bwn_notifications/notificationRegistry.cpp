#include "precompiled_notifications.hpp"

#include "bwn_notifications/notificationRegistry.hpp"

void bwn::NotificationGodotInterface::subscribeGlobal(NotificationSubscriberNode& _subscriber, const String& _name) const
{
	m_notificationGlobalSubscriber(_subscriber, _name);
}

void bwn::NotificationGodotInterface::subscribeForNode(NotificationSubscriberNode& _subscriber, const Node& _node, const String& _name) const
{
	m_notificationNodeSubscriber(_subscriber, _node, _name);
}

void bwn::NotificationGodotInterface::unsubscribeAll(NotificationSubscriberNode& _subscriber) const
{
	m_notificationUnsubscriber(_subscriber);
}

bwn::NotificationRegistry::NotificationRegistry() = default;
bwn::NotificationRegistry::~NotificationRegistry() = default;

const bwn::NotificationGodotInterface* bwn::NotificationRegistry::findInterface(const StringId& _id) const
{
	RegistrationMap::const_iterator it = m_registrationMap.find(_id);

	if (it != m_registrationMap.end())
	{
		return &(it->second);	
	}

	return nullptr;
}