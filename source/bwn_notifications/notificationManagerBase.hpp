#pragma once

#include "bwn_core/functional/callback.hpp"
#include "bwn_core/thread/spinlock.hpp"

#include <scene/main/node.h>

#include <vector>
#include <mutex>

namespace bwn
{
	class NotificationBase;

	class NotificationManagerBase : public Node
	{
		GDCLASS(NotificationManagerBase, Node);

		//
		// Private structs.
		//
	private:
		struct NotificationRecord;

		//
		// Construction and destruction.
		//
	public:
		NotificationManagerBase(const int _processNotification);
		virtual ~NotificationManagerBase() override;

		//
		// Godot methods.
		//
	protected:
		void _notification(const int _notification);

		//
		// Public interface.
		//
	public:
		void pushNotification(const Ref<NotificationBase>& _notification, const Node*const _node);

		//
		// Private methods.
		//
	private:
		void process();

		//
		// Private members.
		//
	private:
		// Stack of the pushed notifications, which are waiting to be processed.
		std::vector<NotificationRecord> m_stack;
		std::vector<NotificationRecord> m_cacheStack;
		bwn::spinlock m_stackLock;
		int m_processNotification;
	};
} // namespace bwn