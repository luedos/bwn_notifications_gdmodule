#include "precompiled_notifications.hpp"

#include "bwn_notifications/notificationSubscriberNode.hpp"
#include "bwn_notifications/notificationRegistry.hpp"

namespace names
{
	// This is mostly done, to not copy string each time we emmit signal.
	static constexpr const char* k_onNotificationSignal = "notification_received";
}

bwn::NotificationSubscriberNode::NotificationSubscriberNode() = default;
bwn::NotificationSubscriberNode::~NotificationSubscriberNode() = default;

void bwn::NotificationSubscriberNode::_bind_methods()
{
	bwn::addSignal<NotificationSubscriberNode>(
		MethodUtility<void(const Ref<NotificationBase>&)>(names::k_onNotificationSignal)
			.argName<0>("notification_object")
			.get());
}

void bwn::NotificationSubscriberNode::_notification(const int _notification)
{
	switch(_notification)
	{
		case NOTIFICATION_ENTER_TREE:
		{
			if (!m_notificationType.is_empty())
			{
				subscribe();
			}
		}
		break;

		case NOTIFICATION_EXIT_TREE:
		{
			if (!m_notificationType.is_empty())
			{
				unsubscribe();
			}
		}
		break;
	}
}

void bwn::NotificationSubscriberNode::setNotificationType(const String& _type)
{
	if (m_notificationType == _type)
	{
		return;
	}

	if (is_inside_tree() && !m_notificationType.is_empty())
	{
		unsubscribe();
	}

	m_notificationType = _type;

	if (is_inside_tree() && !m_notificationType.is_empty())
	{
		subscribe();
	}
}

const String& bwn::NotificationSubscriberNode::getNotificationType() const
{
	return m_notificationType;
}

void bwn::NotificationSubscriberNode::setNotificationName(const String& _name)
{
	if (m_notificationName == _name)
	{
		return;
	}

	if (is_inside_tree() && !m_notificationType.is_empty())
	{
		unsubscribe();
	}

	m_notificationName = _name;

	if (is_inside_tree() && !m_notificationType.is_empty())
	{
		subscribe();
	}
}

const String& bwn::NotificationSubscriberNode::getNotificationName() const
{
	return m_notificationName;
}

void bwn::NotificationSubscriberNode::setSubscribeGlobal(const bool _global)
{
	if (m_subscribeGlobal == _global)
	{
		return;
	}

	if (is_inside_tree() && !m_notificationType.is_empty())
	{
		unsubscribe();
	}

	m_subscribeGlobal = _global;

	if (is_inside_tree() && !m_notificationType.is_empty())
	{
		subscribe();
	}
}

bool bwn::NotificationSubscriberNode::getSubscribeGlobal() const
{
	return m_subscribeGlobal;
}

void bwn::NotificationSubscriberNode::signal(const Ref<NotificationBase>& _notification)
{
	emit_signal(SNAME(names::k_onNotificationSignal), _notification);
}

void bwn::NotificationSubscriberNode::unsubscribe()
{
	const NotificationGodotInterface*const interface = NotificationRegistry::getInstance()->findInterface(StringId(bwn::wrap_view(m_notificationType)));
	BWN_ASSERT_WARNING_COND(interface != nullptr, "The notification {} wasn't registered.", m_notificationType);

	if (interface != nullptr)
	{
		interface->unsubscribeAll(*this);
	}
}

void bwn::NotificationSubscriberNode::subscribe()
{
	const NotificationGodotInterface*const interface = NotificationRegistry::getInstance()->findInterface(StringId(bwn::wrap_view(m_notificationType)));
	BWN_ASSERT_WARNING_COND(interface != nullptr, "The notification {} wasn't registered.", m_notificationType);

	if (interface == nullptr)
	{
		return;
	}

	if (m_subscribeGlobal)
	{
		interface->subscribeGlobal(*this, m_notificationName);
	}
	else
	{
		const Node*const parent = get_parent();
		BWN_TRAP_COND(parent != nullptr, "Trying subscribe to in node without parent.");
		interface->subscribeForNode(*this, *parent, m_notificationName);
	}
}