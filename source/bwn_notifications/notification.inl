#pragma once

#include "bwn_notifications/notification.hpp"

#include "bwn_core/thread/fakelock.hpp"

#include <utility>
#include <mutex>

namespace bwn::detail
{
	template<typename NotificationT>
	struct IsBaseNotification : public std::false_type
	{};

	template<>
	struct IsBaseNotification<NotificationBase> : public std::true_type
	{};

	template<typename NotificationManagerT>
	struct IsBaseNotification<Notification<NotificationManagerT>> : public std::true_type
	{};

} // namespace bwn::detail

template<typename NotificationT, typename...ArgTs>
void bwn::signalNotification(const Node*const _node, ArgTs&&..._args)
{
	bwn::instantiateRef<NotificationT>(std::forward<ArgTs>(_args)...)->signal(_node);
}

template<typename NotificationT, typename...ArgTs>
void bwn::signalNotificationImmediate(const Node*const _node, ArgTs&&..._args)
{
	bwn::instantiateRef<NotificationT>(std::forward<ArgTs>(_args)...)->signalImmediate(_node);
}

template<typename NotificationClassT, typename NotificationBaseClassT>
struct bwn::NotificationInterface<NotificationClassT, NotificationBaseClassT>::Subscription
{
	//
	// Construction and destruction.
	//
public:
	Subscription(const NotificationCallback& _callback, const Object& _listener, const String& _name, const Node*const _node)
		: m_callback( _callback )
		, m_listener( &_listener )
		, m_node( _node )
		, m_notificationName( _name )
	{}

	//
	// Public interface.
	//
public:
	// Return true if provided name of an event is valid for the listener.
	bool isNameValid(const bwn::string_view& _name) const
	{
		return m_notificationName.is_empty() || bwn::wrap_view(m_notificationName) == _name;
	}
	// Returns listener.
	const Object* getListener() const
	{
		return m_listener;
	}
	// Returns name for the notification.
	const String& getNotificationName() const
	{
		return m_notificationName;
	}
	// Returns node to which notification was subscribed.
	bool isNodeValid(const Node*const _node) const
	{
		return m_node == nullptr || m_node == _node || m_node->is_ancestor_of(_node);
	}
	// Returns node to which notification was subscribed.
	const Node* getNode() const
	{
		return m_node;
	}
	// Signals the callback with provided notification
	void signal(const Ref<NotificationClassT>& _notification) const
	{
		m_callback.invoke(_notification);
	}

	//
	// Private members.
	//
private:
	NotificationCallback m_callback;
	const Object* m_listener = nullptr;
	const Node* m_node = nullptr;
	String m_notificationName;
};

template<typename NotificationClassT, typename NotificationBaseClassT>
struct bwn::NotificationInterface<NotificationClassT, NotificationBaseClassT>::SubscriptionsData
{
	//
	// Public interface.
	//
public:
	// Adds global subscription for the listener with the specific callback.
	void addSubscription(const Object& _listener, const NotificationCallback& _callback, const String& _name)
	{
		m_subscriptions.emplace_back(_callback, _listener, _name, nullptr);
	}
	// Adds subscription connected to specific node for the specific listener with the callback.
	void addNodeSubscription(const Object& _listener, const Node& _node, const NotificationCallback& _callback, const String& _name)
	{
		m_subscriptions.emplace_back(_callback, _listener, _name, &_node);
	}

	// Remove all callbacks for the specific listener.
	void removeListener(const Object& _listener)
	{
		const typename std::vector<Subscription>::iterator firstRemovedIt = std::remove_if(
			m_subscriptions.begin(),
			m_subscriptions.end(),
			[&_listener](const Subscription& _subscription) { return _subscription.getListener() == &_listener; });

		m_subscriptions.erase(firstRemovedIt, m_subscriptions.end());
	}
	// Removes specific global callback which listens for the notification with specific name (or general if no name is provided).
	// Doesn't remove any other callbacks.
	void removeGlobalSubscription(const Object& _listener, const bwn::string_view& _name)
	{
		const typename std::vector<Subscription>::iterator firstRemovedIt = std::remove_if(
			m_subscriptions.begin(),
			m_subscriptions.end(),
			[&_listener, &_name](const Subscription& _subscription)
			{ 
				return _subscription.getListener() == &_listener
					&& _subscription.getNode() == nullptr
					&& bwn::wrap_view(_subscription.getNotificationName()) == _name;
			});
		
		BWN_ASSERT_WARNING_COND(
			firstRemovedIt != m_subscriptions.end(),
			"No subscriptions of notification '{}' with name '{}' to remove for listener '{}'.",
			NotificationClassT::get_class_static(),
			bwn::wrap_utf(_name),
			_listener.get_class());

		m_subscriptions.erase(firstRemovedIt, m_subscriptions.end());
	}
	// Removes specific callback on a node which listens for the notification with specific name (or general if no name is provided).
	// Doesn't remove any other callbacks.
	void removeNodeSubscription(const Object& _listener, const Node& _node, const bwn::string_view& _name)
	{
		const typename std::vector<Subscription>::iterator firstRemovedIt = std::remove_if(
			m_subscriptions.begin(),
			m_subscriptions.end(),
			[&_listener, &_name, &_node](const Subscription& _subscription)
			{ 
				return _subscription.getListener() == &_listener
					&& _subscription.getNode() == &_node
					&& bwn::wrap_view(_subscription.getNotificationName()) == _name;
			});
		
		BWN_ASSERT_WARNING_COND(
			firstRemovedIt != m_subscriptions.end(),
			"No subscriptions of notification '{}' with name '{}' connected to node '{}' to remove for listener '{}'.",
			NotificationClassT::get_class_static(),
			bwn::wrap_utf(_name),
			debug::getNodePath(&_node),
			_listener.get_class());
	
		m_subscriptions.erase(firstRemovedIt, m_subscriptions.end());
	}

	// Returns global subscriptions.
	const std::vector<Subscription>& getSubscriptions() const
	{
		return m_subscriptions;
	}

#if defined(DEBUG_ENABLED)
	bwn::fakelock& getLock()
	{
		return m_lock;
	}
#endif

	//
	// Private members.
	//
private:
	// Global subscriptions which doesn't connected to any nodes.
	std::vector<Subscription> m_subscriptions;
#if defined(DEBUG_ENABLED)
	bwn::fakelock m_lock;
#endif
};

template<typename NotificationClassT, typename NotificationBaseClassT>
void bwn::NotificationInterface<NotificationClassT, NotificationBaseClassT>::subscribeGlobal(const Object& _listener, const NotificationCallback& _callback, const String& _name)
{
	BWN_TRAP_COND(_callback.is_valid(), "Invalid callback will lead to the sigfault later.");

	SubscriptionsData& data = getSubscriptionsData();
	BWN_FAKE_SCOPE_LOCK(data.getLock());

	data.addSubscription(_listener, _callback, _name);
}

template<typename NotificationClassT, typename NotificationBaseClassT>
void bwn::NotificationInterface<NotificationClassT, NotificationBaseClassT>::subscribeForNode(const Object& _listener, const Node& _node, const NotificationCallback& _callback, const String& _name)
{
	BWN_TRAP_COND(_callback.is_valid(), "Invalid callback will lead to the sigfault later.");
	
	SubscriptionsData& data = getSubscriptionsData();
	BWN_FAKE_SCOPE_LOCK(data.getLock());

	data.addNodeSubscription(_listener, _node, _callback, _name);	
}

template<typename NotificationClassT, typename NotificationBaseClassT>
void bwn::NotificationInterface<NotificationClassT, NotificationBaseClassT>::unsubscribeAll(const Object& _listener)
{
	SubscriptionsData& data = getSubscriptionsData();
	BWN_FAKE_SCOPE_LOCK(data.getLock());

	data.removeListener(_listener);	
}

template<typename NotificationClassT, typename NotificationBaseClassT>
void bwn::NotificationInterface<NotificationClassT, NotificationBaseClassT>::unsubscribeGlobal(const Object& _listener, const bwn::string_view& _name)
{
	SubscriptionsData& data = getSubscriptionsData();
	BWN_FAKE_SCOPE_LOCK(data.getLock());

	data.removeGlobalSubscription(_listener, _name);
}

template<typename NotificationClassT, typename NotificationBaseClassT>
void bwn::NotificationInterface<NotificationClassT, NotificationBaseClassT>::unsubscribeFromNode(const Object& _listener, const Node& _node, const bwn::string_view& _name)
{
	SubscriptionsData& data = getSubscriptionsData();
	BWN_FAKE_SCOPE_LOCK(data.getLock());

	data.removeNodeSubscription(_listener, _node, _name);
}

template<typename NotificationClassT, typename NotificationBaseClassT>
void bwn::NotificationInterface<NotificationClassT, NotificationBaseClassT>::signalImmediateStatic(NotificationClassT& _notification, const Node*const _node)
{
	{
		const bwn::string_view nameView = bwn::wrap_view(_notification.getNotificationName());
		const Ref<NotificationClassT> notificationRef = Ref<NotificationClassT>(&_notification);
		SubscriptionsData& data = getSubscriptionsData();

		BWN_FAKE_SCOPE_LOCK(data.getLock());

		for (const Subscription& subscription : data.getSubscriptions())
		{
			if (subscription.isNodeValid(_node) && subscription.isNameValid(nameView))
			{
				subscription.signal(notificationRef);
			}
		}
	}

	if constexpr (!detail::IsBaseNotification<NotificationBaseClassT>::value)
	{
		NotificationBaseClassT::signalImmediateStatic(_notification, _node);
	}
}

template<typename NotificationClassT, typename NotificationBaseClassT>
void bwn::NotificationInterface<NotificationClassT, NotificationBaseClassT>::signalImmediateV(const Node*const _node)
{
	signalImmediateStatic(static_cast<NotificationClassT&>(*this), _node);
}

template<typename NotificationClassT, typename NotificationBaseClassT>
typename bwn::NotificationInterface<NotificationClassT, NotificationBaseClassT>::SubscriptionsData& bwn::NotificationInterface<NotificationClassT, NotificationBaseClassT>::getSubscriptionsData()
{
	static SubscriptionsData s_instance;
	return s_instance;
}

template<typename NotificationManagerT>
void bwn::Notification<NotificationManagerT>::signalV(const Node*const _node)
{
	NotificationManagerT::getInstance()->pushNotification(Ref<NotificationBase>(this), _node);
}