#pragma once

#include "bwn_notifications/notificationRegistry.hpp"
#include "bwn_notifications/notificationSubscriberNode.hpp"
#include "bwn_notifications/notification.hpp"
#include "bwn_core/debug/debug.hpp"
#include "bwn_core/debug/debugUtility.hpp"

template<typename NotificationT>
bwn::NotificationGodotInterface::SignalConverterFunctor<NotificationT>::SignalConverterFunctor(NotificationSubscriberNode& _subscriber)
	: m_subscriber( &_subscriber )
{}

template<typename NotificationT>
void bwn::NotificationGodotInterface::SignalConverterFunctor<NotificationT>::operator()(const Ref<NotificationT>& _notification) const
{
	m_subscriber->signal(_notification);
}

template<typename NotificationT>
bool bwn::NotificationGodotInterface::SignalConverterFunctor<NotificationT>::operator==(const SignalConverterFunctor& _other) const
{
	return m_subscriber == _other.m_subscriber;
}

template<typename NotificationT>
bwn::NotificationGodotInterface bwn::NotificationGodotInterface::createInterface()
{
	NotificationGodotInterface interface;
	interface.m_notificationGlobalSubscriber = &NotificationGodotInterface::notificationGlobalSubscriberTemplate<NotificationT>;
	interface.m_notificationNodeSubscriber = &NotificationGodotInterface::notificationNodeSubscriberTemplate<NotificationT>;
	interface.m_notificationUnsubscriber = &NotificationGodotInterface::notificationUnsubscriberTemplate<NotificationT>;

	return interface;
}
template<typename NotificationT>
void bwn::NotificationGodotInterface::notificationGlobalSubscriberTemplate(NotificationSubscriberNode& _subscriber, const String& _name)
{
	NotificationT::subscribeGlobal(_subscriber, SignalConverterFunctor<NotificationT>(_subscriber), _name);
}

template<typename NotificationT>
void bwn::NotificationGodotInterface::notificationNodeSubscriberTemplate(NotificationSubscriberNode& _subscriber, const Node& _node, const String& _name)
{
	NotificationT::subscribeForNode(_subscriber, _node, SignalConverterFunctor<NotificationT>(_subscriber), _name);
}

template<typename NotificationT>
void bwn::NotificationGodotInterface::notificationUnsubscriberTemplate(NotificationSubscriberNode& _subscriber)
{
	NotificationT::unsubscribeAll(_subscriber);
}