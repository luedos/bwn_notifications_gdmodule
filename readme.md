## Simple high level notifications for the godot engine

This library introduce simple notification for the godot engine which can be inherited, be sent globally on a specific node and it's ancestors.

This modules is based on cmake build system implementation and not the scons one.

Other dependencies for this library include:
- bwn_core_gdmodule